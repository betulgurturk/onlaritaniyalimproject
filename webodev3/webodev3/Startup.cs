﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(webodev3.Startup))]
namespace webodev3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
